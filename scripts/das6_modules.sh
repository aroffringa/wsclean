module purge

module load spack/20250109

module load gcc
module load cmake
module load boost
module load casacore
module load cfitsio
module load fftw
module load hdf5
module load openblas
module load openmpi
module load gsl

module load python
module load py-astropy
module load py-astropy-iers-data
module load py-casacore
module load py-h5py
module load py-iniconfig
module load py-numpy
module load py-packaging
module load py-pluggy
module load py-pyerfa
module load py-pytest
module load py-pyyaml
module load py-six

module load dp3
module load everybeam
module load idg
